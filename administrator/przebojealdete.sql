-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Czas generowania: 04 Kwi 2021, 16:06
-- Wersja serwera: 8.0.18
-- Wersja PHP: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `przebojealdete`
--
CREATE DATABASE IF NOT EXISTS `przebojealdete` DEFAULT CHARACTER SET utf8 COLLATE utf8_polish_ci;
USE `przebojealdete`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `galeria`
--

CREATE TABLE `galeria` (
  `ID` int(11) NOT NULL,
  `OKLADKA` int(11) NOT NULL,
  `TYTUL` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `news`
--

CREATE TABLE `news` (
  `ID_NEWS` int(11) NOT NULL,
  `TYTUL` text COLLATE utf8_polish_ci NOT NULL,
  `TRESC_SKROCONA` text COLLATE utf8_polish_ci NOT NULL,
  `TRESC_PELNA` text COLLATE utf8_polish_ci NOT NULL,
  `DATA_PUBLIKACJI` datetime NOT NULL,
  `ID_ZDJECIA` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `news`
--

INSERT INTO `news` (`ID_NEWS`, `TYTUL`, `TRESC_SKROCONA`, `TRESC_PELNA`, `DATA_PUBLIKACJI`, `ID_ZDJECIA`) VALUES
(9, 'tytul', 'tresc skrocona', 'tresc pełna', '2021-04-04 09:27:04', 123),
(10, 'tytul3', 'tresc skrocona', 'tresc pełna', '2021-04-04 09:27:04', 123),
(11, 'tytul2', 'tresc skrocona', 'tresc pełna', '2021-04-04 09:28:04', 123),
(13, 'tytul', 'tresc skrocona', 'tresc pełna', '2022-02-02 12:12:12', 123),
(14, 'tytul', 'tresc skrocona', 'tresc pełna', '2021-04-04 11:50:25', 123),
(15, 'tytul', 'tresc skrocona', 'tresc pełna', '2021-04-05 12:12:12', 123);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zdjecie`
--

CREATE TABLE `zdjecie` (
  `ID` int(11) NOT NULL,
  `SCIEZKA` text COLLATE utf8_polish_ci NOT NULL,
  `TEKST_ALTERNATYWNY` text COLLATE utf8_polish_ci NOT NULL,
  `ID_GALERII` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `galeria`
--
ALTER TABLE `galeria`
  ADD PRIMARY KEY (`ID`);

--
-- Indeksy dla tabeli `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`ID_NEWS`);

--
-- Indeksy dla tabeli `zdjecie`
--
ALTER TABLE `zdjecie`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `galeria`
--
ALTER TABLE `galeria`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `news`
--
ALTER TABLE `news`
  MODIFY `ID_NEWS` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT dla tabeli `zdjecie`
--
ALTER TABLE `zdjecie`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
