<?php
include_once "header2.php";

$newsy = News::zwrocWszystkieNewsy();
?>
<div id="demo"></div>
<!-- AKTUALNOŚCI / PLAYER-->
<section class="page-section" id="aktualnosci">
    <div class="container">
        <div class="row">
            <!-- AKTUALNOŚCI -->
            <div class="container-fluid float-left">
                <h2 class="py-4 section-heading text-uppercase text-center">Galeria</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">Strona główna</a></li>
                    <li class="breadcrumb-item" aria-current="page">Galeria</li>
                </ol>

                <div class="test123">
                  <div class="card-group">
                    <div class="lightgallery">

                      <a href="placeholder.jpg">
                        <div class="card wow animate__animated animate__zoomIn" data-wow-delay="0.2s"  data-sr-item="feature" style="visibility: visible;margin: 6px;">
                          <div class="powidok"><h2>Album 1</h2></div>
                          <img class="card-img-top test" src="placeholder.jpg" alt="Card image cap">
                        </div>
                      </a>

                      <a href="placeholder.jpg">
                        <div class="card wow animate__animated animate__zoomIn" data-wow-delay="0.2s"  data-sr-item="feature" style="visibility: visible;margin: 6px;">
                          <div class="powidok"><h2>Album 2</h2></div>
                          <img class="card-img-top test" src="placeholder.jpg" alt="Card image cap">
                        </div>
                      </a>

                    </div>
                  </div>
                </div>


</div>
</div>
</div>
</section>
<!-- Modal 1-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery-js/1.4.0/js/lightgallery.min.js"></script>

<script type="text/javascript">
    lightGallery(document.querySelector('.lightgallery'));
</script>
<!-- Footer-->
<?php
include_once "footer.php"
?>
