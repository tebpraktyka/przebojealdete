<?php
include_once "header2.php";

$newsy = News::zwrocWszystkieNewsy();
?>
<div id="demo"></div>
<!-- AKTUALNOŚCI / PLAYER-->
<section class="page-section" id="aktualnosci">
    <div class="container">
        <div class="row">
            <!-- AKTUALNOŚCI -->
            <div class="container-fluid float-left">
                <h2 class="py-4 section-heading text-uppercase text-center">Galeria</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">Strona główna</a></li>
                    <li class="breadcrumb-item" aria-current="page">Galeria</li>
                </ol>
                  <div class="card-group">
                    <div class="card wow animate__animated animate__zoomIn" data-wow-delay="0.2s"  data-sr-item="feature" style="visibility: visible;margin: 6px;">
                      <div class="powidok"><h2>Album 1</h2></div>
                      <img class="card-img-top test" src="placeholder.jpg" alt="Card image cap">
                    </div>
                    <div class="card wow animate__animated animate__zoomIn" data-wow-delay="0.4s"  data-sr-item="feature" style="visibility: visible;margin: 6px;">
                      <div class="powidok"><h2>Album 2</h2></div>
                      <img class="card-img-top test" src="placeholder.jpg" alt="Card image cap">
                    </div>
                    <div class="card wow animate__animated animate__zoomIn" data-wow-delay="0.6s"  data-sr-item="feature" style="visibility: visible;margin: 6px;">
                      <div class="powidok"><h2>Album 3</h2></div>
                      <img class="card-img-top test" src="placeholder.jpg" alt="Card image cap">
                    </div>
                  </div>
                  <div class="card-group">
                    <div class="card wow animate__animated animate__zoomIn" data-wow-delay="0.8s"  data-sr-item="feature" style="visibility: visible;margin: 6px;">
                      <div class="powidok"><h2>Album 4</h2></div>
                      <img class="card-img-top test" src="placeholder.jpg" alt="Card image cap">
                    </div>
                    <div class="card wow animate__animated animate__zoomIn" data-wow-delay="1.0s"  data-sr-item="feature" style="visibility: visible;margin: 6px;">
                      <div class="powidok"><h2>Album 5</h2></div>
                      <img class="card-img-top test" src="placeholder.jpg" alt="Card image cap">
                    </div>
                    <div class="card wow animate__animated animate__zoomIn" data-wow-delay="1.2s"  data-sr-item="feature" style="visibility: visible;margin: 6px;">
                      <div class="powidok"><h2>Album 6</h2></div>
                      <img class="card-img-top test" test src="placeholder.jpg" alt="Card image cap">
                    </div>
                  </div>
                  <div class="card-group">
                    <div class="card wow animate__animated animate__zoomIn" data-wow-delay="1.4s"  data-sr-item="feature" style="visibility: visible;margin: 6px;">
                      <div class="powidok"><h2>Album 7</h2></div>
                      <img class="card-img-top test" src="placeholder.jpg" alt="Card image cap">
                    </div>
                    <div class="card wow animate__animated animate__zoomIn" data-wow-delay="1.6s"  data-sr-item="feature" style="visibility: visible;margin: 6px;">
                      <div class="powidok"><h2>Album 8</h2></div>
                      <img class="card-img-top test" src="placeholder.jpg" alt="Card image cap">
                    </div>
                    <div class="card wow animate__animated animate__zoomIn" data-wow-delay="1.8s"  data-sr-item="feature" style="visibility: visible;margin: 6px;">
                      <div class="powidok"><h2>Album 9</h2></div>
                      <img class="card-img-top test" src="placeholder.jpg" alt="Card image cap">
                    </div>
                  </div>




</div>
    </div>
</section>
<!-- Modal 1-->

<!-- Footer-->
<?php
include_once "footer.php"
?>
