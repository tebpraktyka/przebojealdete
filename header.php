<?php
include_once "includes.php"

?>
<!DOCTYPE html>
<html lang="pl">

<head>
    <meta charset="utf-8"/>
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <title>Konkurs Aldete</title>
    <link href="assets/img/favicon.ico" rel="icon" type="image/x-icon"/>
    <!-- Font Awesome icons (free version)-->
    <script crossorigin="anonymous" src="https://use.fontawesome.com/releases/v5.15.1/js/all.js"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet"
          type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css"/>
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/styles.css" rel="stylesheet"/>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-98395286-5"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-98395286-5');
    </script>
</head>

<body id="page-top">
<!-- Navigation-->
<nav class="menuColor navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index.php#page-top"><img alt="" src="assets/img/logo2.png"/></a>
        <button aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"
                class="navbar-toggler navbar-toggler-right" data-target="#navbarResponsive" data-toggle="collapse"
                type="button">
                    Menu
            <i class="fas fa-bars ml-1"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav text-uppercase ml-auto text-center">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php#aktualnosci">Aktualności</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php#polplayback">Półplaybacki</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php#opis">Opis konkursu</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php#doPobrania">Do pobrania</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php#nagrody">Nagrody</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php#organizatorzy">Organizatorzy</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php#kontakt">Kontakt</a></li>
            </ul>
        </div>
    </div>
</nav>
