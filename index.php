<?php
include_once "header.php";
$news = News::zwrocNOstatnich(3);
?>

<!-- LOGO -->
<header class="">
    <div class="carousel slide" data-ride="carousel" id="main-slide">
        <ol class="carousel-indicators">
            <li class="active" data-slide-to="0" data-target="#main-slide"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img alt="First slide" class="d-block w-100 masthead img-fluid" src="assets/img/aldete/tlo.svg">
            </div>
        </div>
    </div>
</header>
<!-- AKTUALNOŚCI / PLAYER-->
<section class="page-section">
    <div class="container">
        <div class="row">
            <!-- AKTUALNOŚCI -->
            <div class="col-lg-6" id="aktualnosci">
                <div class="container-fluid">
                    <h2 class="py-4 section-heading text-uppercase text-center">Aktualności</h2>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card text-white bg-primary mb-3">
                                <div class="card-header"><?php echo $news[0]->getDataPublikacji(); ?></div>
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo $news[0]->getTytul(); ?></h5>
                                    <p class="card-text">
                                        <?php  echo $news[0]->getTrescSkrocona(); ?>
                                    </p>
                                    <p><a class="a-news portfolio-link" data-toggle="modal" href="#news1">Czytaj
                                            więcej...</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="card bg-light mb-3">
                                <div class="card-header"><?php echo $news[1]->getDataPublikacji(); ?></div>
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo $news[1]->getTytul(); ?></h5>
                                    <p class="card-text">
                                        <?php echo $news[1]->getTrescSkrocona(); ?>
                                    </p>
                                    <p><a class="a-news-grey portfolio-link" data-toggle="modal" href="#news2">Czytaj
                                            więcej...</a></p>
                                </div>
                            </div>
                        </div>

			 <!--<div class="col-lg-12">
                            <div class="card text-white bg-primary mb-3">
                                <div class="card-header"><?php //echo $news[2]->getDataPublikacji(); ?></div>
                                <div class="card-body">
                                    <h5 class="card-title"><?php //echo $news[2]->getTytul(); ?></h5>
                                    <p class="card-text"><?php //echo $news[2]->getTrescSkrocona(); ?></p>
                                    <p><a class="a-news portfolio-link" data-toggle="modal" href="#news3">Czytj
                                            więcej...</a></p>
                                </div>
                            </div>
                            <div class="card card-header text-uppercase"><a class="a-news-grey" href="news.php#aktualnosci">zobacz
                                    wszystkie Aktualności...</a></div>
                        </div>
-->
                    </div>
                </div>
            </div>
            <!-- PLAYER -->
            <div class="col-lg-6" id="polplayback">
                <h2 class="py-4 section-heading text-uppercase text-center">Półplaybacki</h2>
                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-header card-header-odd" id="hp1">
                            <h5 class="mb-0">
                                <div data-toggle="collapse" data-target="#cp1" aria-expanded="true"
                                     aria-controls="cp1">
                                    <div class="card-header-title"><h5 class="mb-1">
                                            Wypijmy za błędy
                                        </h5></div>
                                    <div class="card-header-ico"><i aria-hidden="true" class="fa fa-angle-down"></i>
                                    </div>
                                </div>
                            </h5>
                        </div>

                        <div id="cp1" class="collapse show" aria-labelledby="hp1"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <div class="text-center">
                                    <p class="text-justify mb-1">muz. Ryszard Rynkowski, sł. Jacek Cygan, wyk.
                                        Ryszard
                                        Rynkowski</p>
                                    <audio class="m-3" controls controlslist="nodownload" preload="metadata">
                                        <source src="assets/polplayback/mp3/Wypijmy.mp3" type="audio/mpeg">
                                        Twoja przeglądarka nie wspiera odtwarzacza audio.
                                    </audio>
                                </div>
                                <div><a name="np1" onclick="liczPobranie(this)"
                                            class="list-group-item-a" download
                                            href="assets/polplayback/wav/Wypijmy.wav"><i class="fa fa-download"></i>
                                        Pobierz
                                        [WAV, 121MB]</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header card-header-even" id="hp2">
                            <h5 class="mb-0">
                                <div data-toggle="collapse"
                                     data-target="#cp2" aria-expanded="false"
                                     aria-controls="cp2">
                                    <div class="card-header-title"><h5 class="mb-1">
                                            Odkryjemy miłość nieznaną
                                        </h5></div>
                                    <div class="card-header-ico"><i aria-hidden="true" class="fa fa-angle-down"></i>
                                    </div>
                                </div>
                            </h5>
                        </div>
                        <div id="cp2" class="collapse" aria-labelledby="hp2"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <div class="text-center">
                                    <p class="text-justify mb-1">muz. Włodzimierz Korcz, sł. Wojciech Młynarski,
                                        wyk. Alicja Majewska</p>
                                    <audio class="m-3" controls controlslist="nodownload" preload="metadata">
                                        <source src="assets/polplayback/mp3/Odkryjemy.mp3" type="audio/mpeg">
                                        Twoja przeglądarka nie wspiera odtwarzacza audio.
                                    </audio>
                                </div>
                                <div><a name="np2" onclick="liczPobranie(this)"
                                            class="list-group-item-a" download
                                            href="assets/polplayback/wav/Odkryjemy.wav"><i class="fa fa-download"></i>Pobierz
                                        [WAV,
                                        123MB]</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header card-header-odd" id="hp3">
                            <h5 class="mb-0">
                                <div data-toggle="collapse"
                                     data-target="#cp3" aria-expanded="false"
                                     aria-controls="cp3">
                                    <div class="card-header-title"><h5 class="mb-1">
                                            Coś optymistycznego
                                        </h5></div>
                                    <div class="card-header-ico"><i aria-hidden="true" class="fa fa-angle-down"></i>
                                    </div>
                                </div>
                            </h5>
                        </div>
                        <div id="cp3" class="collapse" aria-labelledby="hp3"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <div class="text-center">
                                    <p class="text-justify mb-1">muz. Konstanty Joriadis, sł. Katarzyna Kowalska, wyk.
                                        Kasia Kowalska</p>
                                    <audio class="m-3" controls controlslist="nodownload" preload="metadata">
                                        <source src="assets/polplayback/mp3/Cos_optymistycznego.mp3" type="audio/mpeg">
                                        Twoja przeglądarka nie wspiera odtwarzacza audio.
                                    </audio>
                                </div>
                                <div><a name="np3" onclick="liczPobranie(this)"
                                            class="list-group-item-a" download
                                            href="assets/polplayback/wav/Cos_optymistycznego.wav"><i
                                                class="fa fa-download"></i> Pobierz [WAV, 113MB]</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header card-header-even" id="hp4">
                            <h5 class="mb-0">
                                <div data-toggle="collapse"
                                     data-target="#cp4" aria-expanded="false"
                                     aria-controls="cp4">
                                    <div class="card-header-title"><h5 class="mb-1">
                                            Nic nie może wiecznie trwać
                                        </h5></div>
                                    <div class="card-header-ico"><i aria-hidden="true" class="fa fa-angle-down"></i>
                                    </div>
                                </div>
                            </h5>
                        </div>
                        <div id="cp4" class="collapse" aria-labelledby="hp4"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <div class="text-center">
                                    <p class="text-justify mb-1">muz. Romuald Lipko, sł. Andrzej Mogielnicki, wyk. Anna
                                        Jantar</p>
                                    <audio class="m-3" controls controlslist="nodownload" preload="metadata">
                                        <source src="assets/polplayback/mp3/Nic_nie_moze.mp3" type="audio/mpeg">
                                        Twoja przeglądarka nie wspiera odtwarzacza audio.
                                    </audio>
                                </div>
                                <div><a name="np4" onclick="liczPobranie(this)"
                                            class="list-group-item-a" download
                                            href="assets/polplayback/wav/Nic_nie_moze.wav"><i
                                                class="fa fa-download"></i>
                                        Pobierz [WAV, 101MB]</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header card-header-odd" id="hp5">
                            <h5 class="mb-0">
                                <div data-toggle="collapse"
                                     data-target="#cp5" aria-expanded="false"
                                     aria-controls="cp5">
                                    <div class="card-header-title"><h5 class="mb-1">
                                            Szklana pogoda
                                        </h5></div>
                                    <div class="card-header-ico"><i aria-hidden="true" class="fa fa-angle-down"></i>
                                    </div>
                                </div>
                            </h5>
                        </div>
                        <div id="cp5" class="collapse" aria-labelledby="hp5"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <div class="text-center">
                                    <p class="text-justify mb-1">muz. Grzegorz Stróżniak, sł. Marek Dutkiewicz, wyk.
                                        Lombard</p>
                                    <audio class="m-3" controls controlslist="nodownload" preload="metadata">
                                        <source src="assets/polplayback/mp3/Szklana_pogoda.mp3" type="audio/mpeg">
                                        Twoja przeglądarka nie wspiera odtwarzacza audio.
                                    </audio>
                                </div>
                                <div><a name="np5" onclick="liczPobranie(this)"
                                            class="list-group-item-a" download
                                            href="assets/polplayback/wav/Szklana_pogoda.wav"><i
                                                class="fa fa-download"></i>
                                        Pobierz [WAV, 100MB]</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header card-header-even" id="hp6">
                            <h5 class="mb-0">
                                <div data-toggle="collapse"
                                     data-target="#cp6" aria-expanded="false"
                                     aria-controls="cp6">
                                    <div class="card-header-title"><h5 class="mb-1">
                                            Daj mi tę noc
                                        </h5></div>
                                    <div class="card-header-ico"><i aria-hidden="true" class="fa fa-angle-down"></i>
                                    </div>
                                </div>
                            </h5>
                        </div>
                        <div id="cp6" class="collapse" aria-labelledby="hp6"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <div class="text-center">
                                    <p class="text-justify mb-1">muz. Sławomir Sokołowski, sł. Andrzej Sobczak, wyk.
                                        Bolter</p>
                                    <audio class="m-3" controls controlslist="nodownload" preload="metadata">
                                        <source src="assets/polplayback/mp3/Daj_mi.mp3" type="audio/mpeg">
                                        Twoja przeglądarka nie wspiera odtwarzacza audio.
                                    </audio>
                                </div>
                                <div><a name="np6" onclick="liczPobranie(this)"
                                            class="list-group-item-a" download
                                            href="assets/polplayback/wav/Daj_mi.wav"><i class="fa fa-download"></i>
                                        Pobierz
                                        [WAV, 100MB]</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header card-header-odd" id="hp7">
                            <h5 class="mb-0">
                                <div data-toggle="collapse"
                                     data-target="#cp7" aria-expanded="false"
                                     aria-controls="cp7">
                                    <div class="card-header-title"><h5 class="mb-1">
                                            Radość o poranku
                                        </h5></div>
                                    <div class="card-header-ico"><i aria-hidden="true" class="fa fa-angle-down"></i>
                                    </div>
                                </div>
                            </h5>
                        </div>
                        <div id="cp7" class="collapse" aria-labelledby="hp7"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <div class="text-center">
                                    <p class="text-justify mb-1">muz. Juliusz Loranc, sł. Jonasz Kofta, wyk. Grupa I</p>
                                    <audio class="m-3" controls controlslist="nodownload" preload="metadata">
                                        <source src="assets/polplayback/mp3/Radosc.mp3" type="audio/mpeg">
                                        Twoja przeglądarka nie wspiera odtwarzacza audio.
                                    </audio>
                                </div>
                                <div><a name="np7" onclick="liczPobranie(this)"
                                            class="list-group-item-a" download
                                            href="assets/polplayback/wav/Radosc.wav"><i class="fa fa-download"></i>
                                        Pobierz
                                        [WAV, 104MB]</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header card-header-even" id="hp8">
                            <h5 class="mb-0">
                                <div data-toggle="collapse"
                                     data-target="#cp8" aria-expanded="false"
                                     aria-controls="cp8">
                                    <div class="card-header-title"><h5 class="mb-1">
                                            Zegarmistrz światła
                                        </h5></div>
                                    <div class="card-header-ico"><i aria-hidden="true" class="fa fa-angle-down"></i>
                                    </div>
                                </div>
                            </h5>
                        </div>
                        <div id="cp8" class="collapse" aria-labelledby="hp8"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <div class="text-center">
                                    <p class="text-justify mb-1">muz. Tadeusz Woźniak, sł. Bogdan Chorążuk, wyk. Tadeusz
                                        Woźniak</p>
                                    <audio class="m-3" controls controlslist="nodownload" preload="metadata">
                                        <source src="assets/polplayback/mp3/Zegarmistrz.mp3" type="audio/mpeg">
                                        Twoja przeglądarka nie wspiera odtwarzacza audio.
                                    </audio>
                                </div>
                                <div><a name="np8" onclick="liczPobranie(this)"
                                            class="list-group-item-a" download
                                            href="assets/polplayback/wav/Zegarmistrz.wav"><i class="fa fa-download"></i>
                                        Pobierz [WAV, 124MB]</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header card-header-odd" id="hp9">
                            <h5 class="mb-0">
                                <div data-toggle="collapse"
                                     data-target="#cp9" aria-expanded="false"
                                     aria-controls="cp9">
                                    <div class="card-header-title"><h5 class="mb-1">
                                            W siną dal
                                        </h5></div>
                                    <div class="card-header-ico"><i aria-hidden="true" class="fa fa-angle-down"></i>
                                    </div>
                                </div>
                            </h5>
                        </div>
                        <div id="cp9" class="collapse" aria-labelledby="hp9"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <div class="text-center">
                                    <p class="text-justify mb-1">muz. Jerzy Duduś Matuszkiewicz, sł. Bogusław Choiński,
                                        Jan Gałkowski,
                                        wyk. Bohdan Łazuka oraz Iga Cembrzyńska</p>
                                    <audio class="m-3" controls controlslist="nodownload" preload="metadata">
                                        <source src="assets/polplayback/mp3/W_sina_dal.mp3" type="audio/mpeg">
                                        Twoja przeglądarka nie wspiera odtwarzacza audio.
                                    </audio>
                                </div>
                                <div><a name="np9" onclick="liczPobranie(this)"
                                            class="list-group-item-a" download
                                            href="assets/polplayback/wav/W_sina_dal.wav"><i class="fa fa-download"></i>
                                        Pobierz [WAV, 90MB]</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header card-header-even" id="hp10">
                            <h5 class="mb-0">
                                <div data-toggle="collapse"
                                     data-target="#cp10" aria-expanded="false"
                                     aria-controls="cp10">
                                    <div class="card-header-title"><h5 class="mb-1">
                                            Małgośka
                                        </h5></div>
                                    <div class="card-header-ico"><i aria-hidden="true" class="fa fa-angle-down"></i>
                                    </div>
                                </div>
                            </h5>
                        </div>
                        <div id="cp10" class="collapse" aria-labelledby="hp10"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <div class="text-center">
                                    <p class="text-justify mb-1">muz. Katarzyna Gaertner, sł. Agnieszka Osiecka, wyk.
                                        Maryla Rodowicz</p>
                                    <audio class="m-3" controls controlslist="nodownload" preload="metadata">
                                        <source src="assets/polplayback/mp3/Malgoska.mp3" type="audio/mpeg">
                                        Twoja przeglądarka nie wspiera odtwarzacza audio.
                                    </audio>
                                </div>
                                <div><a name="np10" onclick="liczPobranie(this)"
                                            class="list-group-item-a" download
                                            href="assets/polplayback/wav/Malgoska.wav"><i class="fa fa-download"></i>
                                        Pobierz [WAV, 113MB]</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header card-header-odd" id="hp11">
                            <h5 class="mb-0">
                                <div data-toggle="collapse"
                                     data-target="#cp11" aria-expanded="false"
                                     aria-controls="cp11">
                                    <div class="card-header-title"><h5 class="mb-1">
                                            Kolorowe jarmarki
                                        </h5></div>
                                    <div class="card-header-ico"><i aria-hidden="true" class="fa fa-angle-down"></i>
                                    </div>
                                </div>
                            </h5>
                        </div>
                        <div id="cp11" class="collapse" aria-labelledby="hp11"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <div class="text-center">
                                    <p class="text-justify mb-1">muz. Janusz Laskowski, sł. Ryszard Ulicki, wyk. Janusz
                                        Laskowski</p>
                                    <audio class="m-3" controls controlslist="nodownload" preload="metadata">
                                        <source src="assets/polplayback/mp3/Kolorowe.mp3" type="audio/mpeg">
                                        Twoja przeglądarka nie wspiera odtwarzacza audio.
                                    </audio>
                                </div>
                                <div><a name="np11" onclick="liczPobranie(this)"
                                            class="list-group-item-a" download
                                            href="assets/polplayback/wav/Kolorowe.wav"><i class="fa fa-download"></i>
                                        Pobierz [WAV, 169MB]</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header card-header-even" id="hp12">
                            <h5 class="mb-0">
                                <div data-toggle="collapse"
                                     data-target="#cp12" aria-expanded="false"
                                     aria-controls="cp12">
                                    <div class="card-header-title"><h5 class="mb-1">
                                            Boso
                                        </h5></div>
                                    <div class="card-header-ico"><i aria-hidden="true" class="fa fa-angle-down"></i>
                                    </div>
                                </div>
                            </h5>
                        </div>
                        <div id="cp12" class="collapse" aria-labelledby="hp12"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <div class="text-center">
                                    <p class="text-justify mb-1">muz.
                                        Mateusz Pospieszalski, sł. Bartłomiej Kudasik, Sebastian Karpiel, wyk.
                                        Zakopower</p>
                                    <audio class="m-3" controls controlslist="nodownload" preload="metadata">
                                        <source src="assets/polplayback/mp3/Boso.mp3" type="audio/mpeg">
                                        Twoja przeglądarka nie wspiera odtwarzacza audio.
                                    </audio>
                                </div>
                                <div><a name="np12" onclick="liczPobranie(this)"
                                            class="list-group-item-a" download
                                            href="assets/polplayback/wav/Boso.wav"><i class="fa fa-download"></i>
                                        Pobierz
                                        [WAV, 112MB]</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="py-3 text-justify">Podkłady wyłącznie na użytek projektu. Całość bez lektora
                        dostępna na kanale
                        <a href="https://youtu.be/GdcPWghT9NY" target="_blank">YouTube
                            Narodowej Orkiestry Dętej.</a></p>
                </div>
            </div>
        </div>
    </div>


</section>
<!-- OPIS -->
<section class="page-section bg-light" id="opis">
    <div class="container">
        <div class="text-center">
            <h2 class="section-heading text-uppercase">Konkurs</h2>
            <p class="text-justify">Głównym celem konkursu jest autorskie, wokalne wykonanie jednej
                z dwunastu piosenek, do których
                półplaybacki udostępnione są na stronie <a href="https://przebojealdete.pl">www.przebojealdete.pl</a>.
                Piosenki w formacie mp3 zostały zaaranżowane
                przez Mariusza Dziubka, dyrektora Narodowej Orkiestry Dętej w Lubinie. Każdy z uczestników może
                przesłać jedno zgłoszenie. Piosenka wraz z wykonaniem wokalnym i obrazem powinna być nagrana w
                formacie mp4 lub mov. Sugerujemy, by nagranie było wykonane na jasnym, jednolicie oświetlonym tle.
            </p>
            <p class="text-justify">
                Zgłoszenia należy wysyłać do 30 czerwca 2021 r.na adres mailowy: <a
                        href="mailto:aldete@muzeumpiosenki.pl">aldete@muzeumpiosenki.pl</a>.
            </p>
            <p class="text-justify">
                Nagrania zostaną ocenione przez komisję konkursową do 12 lipca 2021 r. Dziesięciu laureatów otrzyma
                zaproszenie do wzięcia udziału w dwudniowych warsztatach wokalnych (1-2.08.2021 r. w Muzeum Polskiej
                Piosenki w Opolu) prowadzonych przez Elżbietę Zapendowską oraz muzyków Narodowej Orkiestry Dętej. W
                trakcie zajęć uczestnicy będą pracować nad przygotowaniem przesłanych przez siebie prac (aranżacja,
                układ sceniczny itp.) do występu przed publicznością. Koncert laureatów z towarzyszeniem Narodowej
                Orkiestry Dętej odbędzie się 5 września 2021 r. w centrum Opola. Uczestników oceniać będzie
                profesjonalne jury złożone z muzyków, wokalistów i dziennikarzy, którzy zdecydują, kto otrzyma Grand
                Prix i pozostałe nagrody.
            </p>
            <section class="sectionPobranie" id="doPobrania">
                <h3 class="text-uppercase">Do pobrania</h3>
                <ul class="text-justify">
                    <li><a download href="assets/pliki/Regulamin_Przeboje_Aldete.pdf">Regulamin w formacie .pdf</a>
                    </li>
                    <li><a download href="assets/pliki/Przeboje_aldete_karta_zgloszenia.docx">Karta Zgłoszenia w
                            foramcie .docx</a></li>
                </ul>
            </section>
        </div>
    </div>
</section>
<!-- NAGRODY-->
<section class="page-section" id="nagrody">
    <div class="text-center">
        <h2 class="section-heading text-uppercase">Nagrody</h2>
    </div>
    <div class="py-4">
        <ul class="timeline">
            <li>
                <div class="timeline-image"><img alt="" class="rounded-circle img-fluid"
                                                 src="assets/img/nagrody/sawp.png"/></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4>Grand Prix</h4>
                        <h4 class="subheading">1000 euro</h4>
                    </div>
                    <div class="timeline-body">
                        <p class="text-muted">Grand Prix w wysokości 1000 euro ufundowało Stowarzyszenie Artystów
                            Wykonawców Utworów Muzycznych i Słowno – Muzycznych SAWP</p>
                    </div>
                </div>
            </li>
            <li class="timeline-inverted">
                <div class="timeline-image">
                </div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4>Warsztaty</h4>
                        <h4 class="subheading">wokalne</h4>
                    </div>
                    <div class="timeline-body">
                        <p class="text-muted">Każdy z finałowej dziesiątki uczestników otrzyma nagrodę w postaci
                            udziału w wyjątkowych warsztatach prowadzonych przez Elżbietę Zapendowską oraz Narodową
                            Orkiestrę Dętą pod dyrekcją Mariusza Dziubka.</p>
                    </div>
                </div>

            </li>
        </ul>
    </div>
</section>
<!-- ORGANIZATORZY -->
<section class="page-section bg-light" id="organizatorzy">

    <div class="container">
        <div class="text-center">
            <h2 class="section-heading text-uppercase">Organizatorzy</h2>
        </div>
        <div class="row">
            <div class="offset-lg-1 col-lg-5">
                <div class="team-member">
                    <a href="https://muzeumpiosenki.pl"><img alt=""
                                                             class="mx-auto rounded-circle"
                                                             src="assets/img/organizatorzy/mpp.jpg"/></a>
                    <h4>Muzeum Polskiej Piosenki w Opolu</h4>
                    <p class="text-muted text-justify py-3">Muzeum Polskiej Piosenki w Opolu to
                        centrum informacji o
                        polskiej piosence, które chroni jej dorobek, ze szczególnym uwzględnieniem zarówno
                        wykonawców, jak i twórców. Nowoczesne i interaktywne Muzeum mieści się w opolskim
                        amfiteatrze. Wyróżnia je sposób zwiedzania. Każdy gość otrzymuje audioprzewodnik ze
                        słuchawkami, który łączy się z poszczególnymi elementami ekspozycji i w sposób automatyczny
                        transmituje dźwięk. Głównym elementem Muzeum są muzyczne ściany z folderami artystów,
                        zawierające zdjęcia, teledyski, koncerty itp. Jest też wyspa tabletów z informacjami
                        dotyczącymi artystów. Ponadto jest miejsce poświęcone festiwalowi opolskiemu, muzyczne kule
                        z filmami i wywiadami, szafa z kostiumami gwiazd oraz budki nagraniowe, w których można
                        zrealizować nagranie do wybranej ścieżki dźwiękowej ulubionego utworu, po czym wysłać
                        nagranie za pomocą e-maila. W Muzeum działa też Edukacyjne Studio Nagrań organizujące
                        warsztaty, a także sesje nagraniowe i postprodukcję.</p>
                    <a class="btn btn-dark btn-social mx-2" href="https://muzeumpiosenki.pl" target="_blank"><i
                                class="fas fa-globe"></i></a>
                    <a class="btn btn-dark btn-social mx-2"
                       href="https://www.facebook.com/muzeumpolskiejpiosenki/?fref=ts" target="_blank"><i
                                class="fab fa-facebook-f"></i></a>
                    <a class="btn btn-dark btn-social mx-2"
                       href="https://www.instagram.com/muzeum_polskiej_piosenki/" target="_blank"><i
                                class="fab fa-instagram"></i></a>
                    <a class="btn btn-dark btn-social mx-2"
                       href="https://www.youtube.com/channel/UCMJMjvLuTegQOUn0GQGVmeA" target="_blank"><i
                                class="fab fa-youtube"></i></a>
                </div>
            </div>
            <div class="offset-lg-1 col-lg-5">
                <div class="team-member">
                    <a href="https://nod.lubin.pl"><img alt=""
                                                        class="mx-auto rounded-circle"
                                                        src="assets/img/organizatorzy/nod.png"/></a>
                    <h4>Narodowa Orkiestra Dęta w Lubinie</h4>
                    <p class="text-muted text-justify py-3">Pierwsza polska filharmonia dęta z
                        siedzibą w Lubinie,
                        powołana w 2019 roku z inicjatywy prezydenta Lubina, Roberta Raczyńskiego. Dyrektorem i
                        kierownikiem artystycznym Narodowej Orkiestry Dętej jest Mariusz Dziubek - dyrygent,
                        aranżer, kompozytor oraz producent muzyczny. Autor ponad 150 aranżacji na big-band i
                        orkiestrę dętą. Zespół składa się z etatowych muzyków, jak również współpracuje projektowo z
                        muzykami specjalizującymi się w grze na instrumencie dętym i perkusyjnym. Do współpracy z
                        Narodową Orkiestrą Dętą zapraszani są polscy i zagraniczni artyści oraz soliści z gatunku
                        muzyki klasycznej i rozrywkowej. W repertuarze NOD znajdują się utwory muzyki klasycznej,
                        filmowej, sakralnej, jazzowej oraz rozrywkowej, w aranżacjach na symfoniczną orkiestrę dętą.
                        Na uwagę zasługuje uroczysty koncert z okazji Narodowego Święta Niepodległości, produkcja
                        telewizyjna „Najpiękniejsze kolędy”, Charytatywny Koncert z Narodową, Koncert Noworoczny
                        2021 oraz widowisko muzyczne „Wind(s) on the Hill”. </p>
                    <a class="btn btn-dark btn-social mx-2" href="https://nod.lubin.pl" target="_blank"><i
                                class="fas fa-globe"></i></a>
                    <a class="btn btn-dark btn-social mx-2" href="https://www.facebook.com/nodlubin"
                       target="_blank"><i class="fab fa-facebook-f"></i></a>
                    <a class="btn btn-dark btn-social mx-2" href="https://www.instagram.com/nodlubin"
                       target="_blank"><i class="fab fa-instagram"></i></a>
                    <a class="btn btn-dark btn-social mx-2"
                       href="https://www.youtube.com/channel/UC2fPN_eYM2lsNgkIGLnkACQ" target="_blank"><i
                                class="fab fa-youtube"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- KONTAKT -->
<section class="page-section" id="kontakt">
    <div class="container">
        <div class="text-center">
            <h2 class="section-heading text-uppercase">Kontakt</h2>
            <div class="row">
                <div class="col-lg-6">
                    <h4>Muzeum Polskiej Piosenki w Opolu</h4>
                    <h5>ekspozycja stała</h5>
                    <address>ul. Piastowska 14A<br/>
                        45-082 Opole (Amfiteatr)<br/>
                        rezerwacje: 534 210 205<br/>
                    </address>
                    <h4>Biura Muzeum</h4>
                    <h5>adres do korespondencji</h5>
                    <address>pl. Kopernika 1<br/>
                        45-040 Opole<br/>
                        NIP: 754 296 52 52<br/>
                        tel.: +48 77 441 34 86<br/>
                        e-mail: <a href="mailto:muzeum@muzeumpiosenki.pl">muzeum@muzeumpiosenki.pl</a></address>
                    <h4>Kontakt w sprawach konkursu</h4>
                    <address>
                        Kaja Marchel <br />
                        tel.: 534 210 069 <br />
                        e-mail: <a href="mailto:aldete@muzeumpiosenki.pl">aldete@muzeumpiosenki.pl</a>
                    </address>
                </div>
                <div class="col-lg-6">
                    <h4>Narodowa Orkiestra Dęta</h4>
                    <h5>z siedzibą w Lubinie</h5>
                    <address>
                        ul. Mikołaja Pruzi 7,9<br/>
                        59-300 Lubin<br/>
                        NIP: 692 252 14 43
                    </address>
                    <h4>Sekretariat Orkiestry</h4>
                    <address>
                        poniedziałek-piątek w godz. 08:00-16:00<br/>
                        tel.: +76 307 00 00 <br/>
                        e-mail: <a href="mailto:sekretariat@nod.lubin.pl">sekretariat@nod.lubin.pl</a></address>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Footer-->
<?php
include_once "footer.php"
?>


<!-- Modal 1-->
<div class="news-modal modal fade" id="news1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal"/></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="modal-body">
                            <h2 class="text-uppercase text-break"><?php echo $news[0]->getTytul(); ?></h2>
                            <p class="item-intro text-muted"><?php echo $news[0]->getDataPublikacji(); ?></p>
                            <img class="img-fluid d-block mx-auto" src="assets/zdjecia/14.jpg"/>
                            <div class="text-justify">
                                <?php echo $news[0]->getTrescPelna(); ?>
                            </div>
                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                <i class="fas fa-times mr-1"></i>
                                Zamknij
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal 2-->
<div class="news-modal modal fade" id="news2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal"/></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="modal-body">

                            <h2 class="text-uppercase text-break">Wy pytacie, my odpowiadamy <?php echo $news[1]->getTytul(); ?></h2>
                            <p class="item-intro text-muted text-wrap">16.04.2021 <?php echo $news[1]->getDataPublikacji(); ?></p>
			    <img class="img-fluid d-block mx-auto" src="assets/zdjecia/1.jpg"/>
                            <div class="text-justify">
                                 <?php echo $news[1]->getTrescPelna(); ?>
                            </div>
                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                <i class="fas fa-times mr-1"></i>
                                Zamknij
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--
<div class="news-modal modal fade" id="news3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal"/></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="modal-body">

                            <h2 class="text-uppercase"><?php //echo $news[2]->getTytul(); ?></h2>
                            <p class="item-intro text-muted"><?php //echo $news[2]->getDataPublikacji(); ?></p>
                            <p class="text-justify"><?php //echo $news[2]->getTrescPelna(); ?></p>
                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                <i class="fas fa-times mr-1"></i>
                                Zamknij
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
-->
