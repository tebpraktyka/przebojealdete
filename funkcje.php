<?php
include_once "includes.php";

    if(isset($_POST['nazwa']))
    {
        $n = htmlspecialchars($_POST['nazwa']);

        $n = str_replace("np", "", $n);
        zwiekszPobrania($n);

    }
   else if(isset($_POST['id']))
    {
        $id = htmlentities($_POST['id']);

        $a = News::zwrocNews($id);
        if($a)
        {
            $b['id'] = $a->getId();
            $b['tytul'] = $a->getTytul();
            $b['trescSkrocona'] = $a->getTrescSkrocona();
            $b['trescPelna'] = $a->getTrescPelna();
            $b['dataPublikacji'] = $a->getDataPublikacji();
            $b['idZdjecia'] = $a->getIdZdjecia();

            echo json_encode($b);
        }
        else
        {
            $b['id'] = "";
            $b['tytul'] = "Błąd";
            $b['trescSkrocona'] = "";
            $b['trescPelna'] = "";
            $b['dataPublikacji'] = " ";
            $b['idZdjecia'] = "";

            echo json_encode($b);
        }
    }
    else
    {
        $b['id'] = "";
        $b['tytul'] = "Błąd";
        $b['trescSkrocona'] = "";
        $b['trescPelna'] = "";
        $b['dataPublikacji'] = " ";
        $b['idZdjecia'] = "";

        echo json_encode($b);
    }

    function zwiekszPobrania($id)
    {
        $p = new BazaDanych();
        $p->zapytanieSQL('UPDATE piosenka SET ILOSC_POBRAN = ILOSC_POBRAN + 1 WHERE ID_PIOSENKA = ?', $id);
        $p->close();
    }