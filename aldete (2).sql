-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Czas generowania: 24 Kwi 2021, 15:00
-- Wersja serwera: 8.0.23-0ubuntu0.20.04.1
-- Wersja PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `aldete`
--
CREATE DATABASE IF NOT EXISTS `aldete` DEFAULT CHARACTER SET utf8 COLLATE utf8_polish_ci;
USE `aldete`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `galeria`
--

DROP TABLE IF EXISTS `galeria`;
CREATE TABLE IF NOT EXISTS `galeria` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `OKLADKA` int NOT NULL,
  `TYTUL` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `ID_NEWS` int NOT NULL AUTO_INCREMENT,
  `TYTUL` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `TRESC_SKROCONA` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `TRESC_PELNA` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `DATA_PUBLIKACJI` datetime NOT NULL,
  `ID_ZDJECIA` int DEFAULT NULL,
  PRIMARY KEY (`ID_NEWS`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `news`
--

REPLACE INTO `news` (`ID_NEWS`, `TYTUL`, `TRESC_SKROCONA`, `TRESC_PELNA`, `DATA_PUBLIKACJI`, `ID_ZDJECIA`) VALUES
(16, '„Przeboje al’dete. Konkurs dla wokalistów” 2021 wystartował!', '<p>Ruszyła 1. edycja wydarzenia „Przeboje al’dete. Konkurs dla wokalistów”. Już od dziś czekamy na Wasze autorskie, wokalne wykonanie jednej z dwunastu piosenek, których półplaybacki zaaranżowane przez Mariusza Dziubka, dyrektora Narodowej Orkiestry Dętej, współinicjatora konkursu - wraz z Muzeum Polskiej Piosenki w Opolu - znajdziecie na naszej stronie.\r\n</p>', '<p>Ruszyła 1. edycja wydarzenia „Przeboje al’dete. Konkurs dla wokalistów”. Już od dziś czekamy na Wasze autorskie, wokalne wykonanie jednej z dwunastu piosenek, których półplaybacki zaaranżowane przez Mariusza Dziubka, dyrektora Narodowej Orkiestry Dętej, współinicjatora konkursu - wraz z Muzeum Polskiej Piosenki w Opolu - znajdziecie na naszej stronie.\r\n</p>\r\n<p>Moc instrumentów dętych, muzyka w brzmieniu na żywo, kilkudziesięcioosobowa orkiestra - to wszystko sprawia, że podkłady karaoke tętnią nową energią! Dostrzegam mnóstwo utalentowanych wokalistów, którzy z pasją i radością śpiewają ulubione piosenki. Dzięki podkładom Al’dęte, w duecie z orkiestrą, wykonać mogą największe opolskie przeboje. Obcowanie ze sztuką żywą, instrumentem dętym, wejście w głąb świata dźwięków orkiestry to dla mnie wartość. Zapraszam Was do tego świata!   mówi Mariusz Dziubek.\r\n</p>\r\n<p>Pamiętajcie! Wszystkie prace zgłoszone do konkursu, przejdą przez dwa etapy eliminacji: pierwszy w lipcu, kiedy zgłoszenia oceni komisja konkursowa i zdecyduje, kto weźmie udział w warsztatach wokalnych prowadzonych przez Elżbietę Zapendowską oraz Narodową Orkiestrę Dętą,  oraz drugi, we wrześniu, kiedy ścisłą czołówkę nagrodzi profesjonalne jury, które zdecyduje, kto otrzyma Grand Prix oraz pozostałe nagrody.\r\n</p>\r\n<p>Zachęcamy do zapoznania się z <a download href=\"assets/pliki/Regulamin_Przeboje_Aldete.pdf\">Regulaminem Konkursu.</a></p>\r\n\r\n<p>Czekamy na Was!</p>', '2021-04-07 00:00:00', 1),
(17, 'Wy pytacie, my odpowiadamy', 'Od momentu ogłoszenia konkursu otrzymujemy od Was wiele pytań. Oto trzy, które pojawiają się dosyć często.', '   <p>Od momentu ogłoszenia konkursu otrzymujemy od Was wiele pytań. Oto trzy, które pojawiają się dosyć często.</p>\r\n\r\n                                 <p>\r\n                                    <b>1. Czy w konkursie mogą brać udział wyłącznie soliści?</b><br>\r\n                                    Nie. W konkursie mogą brać udział soliści i duety.\r\n                                 </p>\r\n                                 <p>\r\n                                    <b>2. Czy w nagraniu audio-video dopuszczony jest montaż polegający na wyrównaniu proporcji\r\n                                       wokalu (nagrywanego przez mikrofon zewnętrzny kamery albo wewnętrzny - w smartfonach) i aranżu\r\n                                       (odtwarzanego z osobnego źródła obok kamery) mający na celu poprawę jakości brzmienia\r\n                                       całości w przypadku nagrania kamerą na żywo?</b><br>\r\n                                    Tak. Tego rodzaju montaż jest dopuszczalny.\r\n                                 </p>\r\n                                 <p>\r\n                                    <b>3. Czy praca konkursowa (jednoczesne nagranie dźwięku i obrazu), może podlegać miksowi dźwięku\r\n                                       oraz masteringowi całości z podkładem? Czy wykonanie jakichkolwiek prac studyjnych\r\n                                       dyskwalifikuje z miejsca dane zgłoszenie?</b><br>\r\n                                    Nie można dokonywać prac mikserskich i masteringowych. Możliwe jest jedynie zgranie wokalu z\r\n                                    podkładem i synchronizacja go z video, ustalenie poziomu głośności wokalu w stosunku do\r\n                                    podkładu muzycznego.\r\n                                 </p>\r\n                                 <p>\r\n                                    Czekamy na Wasze zgłoszenia! \r\n                                 </p>', '2021-04-16 00:00:00', 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `piosenka`
--

DROP TABLE IF EXISTS `piosenka`;
CREATE TABLE IF NOT EXISTS `piosenka` (
  `ID_PIOSENKA` int NOT NULL AUTO_INCREMENT,
  `ILOSC_POBRAN` int NOT NULL,
  `TYTUL` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`ID_PIOSENKA`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `piosenka`
--

REPLACE INTO `piosenka` (`ID_PIOSENKA`, `ILOSC_POBRAN`, `TYTUL`) VALUES
(1, 45, 'wypijmy za błędy'),
(2, 53, 'odkryjemy miłość nieznaą'),
(3, 49, 'coś optymistycznego'),
(4, 47, 'nic nie może wiecznie trwać'),
(5, 40, 'szklana pogoda'),
(6, 29, 'daj mi tę noc'),
(7, 41, 'radość o poranku'),
(8, 50, 'zegarmistrz światła'),
(9, 31, 'w siną dal'),
(10, 43, 'Małgośka'),
(11, 34, 'kolorowe jarmarki'),
(12, 35, 'boso');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zdjecie`
--

DROP TABLE IF EXISTS `zdjecie`;
CREATE TABLE IF NOT EXISTS `zdjecie` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `SCIEZKA` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `TEKST_ALTERNATYWNY` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `ID_GALERII` int NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
