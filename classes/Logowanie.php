<?php
include_once "BazaDanych.php";

class Logowanie
{
    private $login;
    private $haslo;
    private $czyZalogowany;

    public function __construct($l, $h)
    {
        $this->login = $l;
        $this->haslo = $h;
    }

    public function zaloguj()
    {
        $p = new BazaDanych();
        $r = $p->select("SELECT COUNT(*) FROM USER WHERE LOGIN = '". $this->login ."' AND PASSWORD = '".$this->haslo."';");
        $p->rozlacz();
        $r = $r->fetch_array(MYSQLI_NUM);

        if($r[0] != 0)
        {
            $this->czyZalogowany = true;
            $_SESSION['login'] = $this->login;
            $_SESSION['zalogowany'] = true;
            return true;
        }
        else return false;
    }

    public static function czyZalogowany()
    {
        session_start();

        if (!$_SESSION['zalogowany']) {
            header("Location: login.php");
            die;
        }
    }

}