<?php
include_once "BazaDanych.php";
include_once "htmlpurifier/library/HTMLPurifier.auto.php";

class News
{
    private $id;
    private $tytul;
    private $trescS;
    private $trescP;
    private $dataP;
    private $idZ;

    /**
     * News constructor.
     * @param $i
     * @param $t
     * @param $ts
     * @param $tp
     * @param $dp
     * @param $iz
     */
    public function __construct($i, $t, $ts, $tp, $dp, $iz)
    {
        $this->id = $i;
        $this->tytul = $t;
        $this->trescS = $ts;
        $this->trescP = $tp;
        $this->dataP = $dp;
        $this->idZ = $iz;
    }

    /**
     * Metoda do doania newsa
     * zwraca true jesli się udało
     * @param $t tytuł
     * @param $ts treść_skrócona
     * @param $tp treść_pełna
     * @param $dp data_publokacji
     * @param $id id_zdjęcia
     * @return bool
     */
    public static function dodajNewsa($t, $ts, $tp, $dp, $idz)
    {
        $p = new BazaDanych();
        return $p->zapytanieSQL('INSERT INTO news (TYTUL, TRESC_SKROCONA, TRESC_PELNA, DATA_PUBLIKACJI, ID_ZDJECIA) VAlUES (?, ?, ?, ?, ?)', $t, $ts, $tp, $dp, $idz);
    }


    public static function zwrocWszystkieNewsy()
    {
        $p = new BazaDanych();
        $r = $p->zapytanieSQL('SELECT * FROM news WHERE DATA_PUBLIKACJI <= NOW() ORDER BY DATA_PUBLIKACJI DESC')->zwrocWszystkie();

        $newsy = array();

        for ($i = 0; $i < count($r); ++$i)
        {
            $n = new News($r[$i]['ID_NEWS'], $r[$i]['TYTUL'], $r[$i]['TRESC_SKROCONA'], $r[$i]['TRESC_PELNA'], $r[$i]['DATA_PUBLIKACJI'], $r[$i]['ID_ZDJECIA']);
            $newsy[] = $n;

        }

        return $newsy;
    }

    public static function zwrocNews($id)
    {
        $p = new BazaDanych();
        $r = $p->zapytanieSQL('SELECT * FROM news WHERE ID_NEWS = ?', $id )->zwrocWszystkie();

        return new News($r[0]['ID_NEWS'], $r[0]['TYTUL'], $r[0]['TRESC_SKROCONA'], $r[0]['TRESC_PELNA'], $r[0]['DATA_PUBLIKACJI'], $r[0]['ID_ZDJECIA']);

    }

    public static function zwrocNOstatnich($ile)
    {
        $p = new BazaDanych();
        $r = $p->zapytanieSQL('SELECT * FROM news WHERE DATA_PUBLIKACJI <= NOW() ORDER BY DATA_PUBLIKACJI DESC LIMIT '.$ile)->zwrocWszystkie();

        $wybraneNewsy = array();

        for ($i = 0; $i < count($r); ++$i)
        {
            $n = new News($r[$i]['ID_NEWS'], $r[$i]['TYTUL'], $r[$i]['TRESC_SKROCONA'], $r[$i]['TRESC_PELNA'], $r[$i]['DATA_PUBLIKACJI'], $r[$i]['ID_ZDJECIA']);
            $wybraneNewsy[] = $n;

        }

        return $wybraneNewsy;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTytul()
    {
        return $this->czyscHTML($this->tytul);
    }

    public function getTrescSkrocona()
    {
        
        return $this->czyscHTML($this->trescS);
    }

    public function getTrescPelna()
    {
        return $this->czyscHTML($this->trescP);
    }

    public function getDataPublikacji()
    {
        return date("d.m.Y", strtotime($this->dataP));
    }

    public function getIdZdjecia()
    {
        return $this->idZ;
    }

    public function setTytul($t)
    {
        $this->tytul = $t;
    }

    public function setTrescSkrocona($ts)
    {
        $this->trescS = $ts;
    }

    public function setTrescPelna($tp)
    {
        $this->trescP = $tp;
    }

    public function setDataPublikcaji($dp)
    {
        $this->dataP = $dp;
    }

    public function setIdZdjecia($iz)
    {
        $this->idZ = $iz;
    }

    public function edytujNewsa()
    {
        $p = new BazaDanych();
        return $p->zapytanieSQL('UPDATE news SET TYTUL = ?, TRESC_SKROCONA = ?, TRESC_PELNA = ?, DATA_PUBLIKACJI = ?, ID_ZDJECIA = ? WHERE ID_NEWS = ?', $this->tytul, $this->trescS, $this->trescP, $this->dataP, $this->idZ, $this->id);
    }

    public function usunNewsa()
    {
        $p = new BazaDanych();
        return $p->zapytanieSQL('DELETE FROM news WHERE ID_NEWS = ?', $this->id);
    }

    private function czyscHTML($h)
    {
        $config = HTMLPurifier_Config::createDefault();
        $config->set('Attr', 'EnableID', true);
                
        $purifier = new HTMLPurifier($config);
        return $purifier->purify($h);
    }
}
//$a = new News(8,"a", "aa","aaa", date('Y-m-d H:i:s'), "null");
//$a->usunNewsa();
/*News::dodajNewsa("tytul", "tresc skrocona", "tresc pełna", date('Y-m-d H:i:s'), 123);
News::dodajNewsa("tytul", "tresc skrocona", "tresc pełna", date('Y-m-d H:i:s'), 123);
News::dodajNewsa("tytul", "tresc skrocona", "tresc pełna", date('Y-m-d H:i:s'), 123);
News::dodajNewsa("tytul", "tresc skrocona", "tresc pełna", date('Y-m-d H:i:s'), 123);
News::dodajNewsa("tytul", "tresc skrocona", "tresc pełna", date('Y-m-d H:i:s'), 123);
News::dodajNewsa("tytul", "tresc skrocona", "tresc pełna", date('Y-m-d H:i:s'), 123);
News::dodajNewsa("tytul", "tresc skrocona", "tresc pełna", date('Y-m-d H:i:s'), 123);
News::dodajNewsa("tytul", "tresc skrocona", "tresc pełna", date('Y-m-d H:i:s'), 123);
*/