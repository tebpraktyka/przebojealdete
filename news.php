<?php
include_once "header2.php";

$newsy = News::zwrocWszystkieNewsy();
?>
<div id="demo"></div>
<!-- AKTUALNOŚCI / PLAYER-->
<section class="page-section" id="aktualnosci">
    <div class="container">
        <div class="row">
            <!-- AKTUALNOŚCI -->
            <div class="container-fluid">
                <h2 class="py-4 section-heading text-uppercase text-center">Aktualności</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">Strona główna</a></li>
                    <li class="breadcrumb-item" aria-current="page">Aktualności</li>
                </ol>
                <?php
                $ileNewsow = count($newsy);
                $ileWRzedzie = 4;
                $ktoryNews = 0;

                for ($i = 0; $ktoryNews < $ileNewsow; ++$i)
                {
                    echo '<div class="row">';
                    if ($i % 2 == 0)
                    {
                        for ($j = 0; $j < $ileWRzedzie && $ktoryNews < $ileNewsow; ++$j, $ktoryNews++)
                        {
                            if ($j % 2 == 0)
                            {
                                echo '
                                       <div class="col-lg-3">
                                            <div class="card text-white bg-primary mb-3">
                                                <div class="card-header">' . $newsy[$ktoryNews]->getDataPublikacji() . '</div>
                                                <div class="card-body">
                                                    <h5 class="card-title">' . $newsy[$ktoryNews]->getTytul() . '</h5>
                                                    <p class="card-text">' . $newsy[$ktoryNews]->getTrescSkrocona() . '</p>
                                                    <p><a onclick="clicked(this)" class="a-news portfolio-link" data-toggle="modal" value="'.$newsy[$ktoryNews]->getId().'"  href="#n1">Czytj
                                                            więcej...</a></p>
                                                </div>
                                            </div>
                                        </div>';
                            }
                            else
                            {
                                echo '
                                       <div class="col-lg-3">
                                            <div class="card bg-light mb-3">
                                                <div class="card-header">' . $newsy[$ktoryNews]->getDataPublikacji() . '</div>
                                                <div class="card-body">
                                                    <h5 class="card-title">' . $newsy[$ktoryNews]->getTytul() . '</h5>
                                                    <p class="card-text">' . $newsy[$ktoryNews]->getTrescSkrocona() . '</p>
                                                    <p><a onclick="clicked(this)" class="a-news-grey portfolio-link" data-toggle="modal" value="'.$newsy[$ktoryNews]->getId().'"  href="#n1">Czytj
                                                            więcej...</a></p>
                                                </div>
                                            </div>
                                        </div>';
                            }

                        }
                    }
                    else
                    {
                        for ($j = 0; $j < $ileWRzedzie && $ktoryNews < $ileNewsow; ++$j, $ktoryNews++)
                        {
                            if ($j % 2 == 0)
                            {
                                echo '
                                       <div class="col-lg-3">
                                            <div class="card bg-light mb-3 ">
                                                <div class="card-header">' . $newsy[$ktoryNews]->getDataPublikacji() . '</div>
                                                <div class="card-body">
                                                    <h5 class="card-title">' . $newsy[$ktoryNews]->getTytul() . '</h5>
                                                    <p class="card-text">' . $newsy[$ktoryNews]->getTrescSkrocona() . '</p>
                                                    <p><a onclick="clicked(this)" class="a-news-grey portfolio-link" data-toggle="modal" value="'.$newsy[$ktoryNews]->getId().'"  href="#n1">Czytj
                                                            więcej...</a></p>
                                                </div>
                                            </div>
                                        </div>';
                            }
                            else
                            {
                                echo '
                                       <div class="col-lg-3">
                                            <div class="card text-white bg-primary mb-3">
                                                <div class="card-header">' . $newsy[$ktoryNews]->getDataPublikacji() . '</div>
                                                <div class="card-body">
                                                    <h5 class="card-title">' . $newsy[$ktoryNews]->getTytul() . '</h5>
                                                    <p class="card-text">' . $newsy[$ktoryNews]->getTrescSkrocona() . '</p>
                                                    <p><a onclick="clicked(this)" class="a-news portfolio-link" data-toggle="modal" value="'.$newsy[$ktoryNews]->getId().'"  href="#n1">Czytj
                                                            więcej...</a></p>
                                                </div>
                                            </div>
                                        </div>';
                            }
                        }
                    }
                    echo '</div>';
                }
                ?>
            </div>
        </div>
    </div>
</section>
<!-- Modal 1-->

<div class="news-modal modal fade" id="n1" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="close-modal" data-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal"></div>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="modal-body">

                                    <h2 class="text-uppercase" id="mTytul"></h2>
                                    <p class="item-intro text-muted" id="mData"></p>
                                    <p class="text-justify" id="mTrescPelna"></p>
                                    <button class="btn btn-primary" data-dismiss="modal" type="button">
                                        <svg class="svg-inline--fa fa-times fa-w-11 mr-1" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" data-fa-i2svg=""><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path></svg><!-- <i class="fas fa-times mr-1"></i> Font Awesome fontawesome.com -->
                                        Zamknij
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>

<!-- Footer-->
<?php
include_once "footer.php"
?>